public interface MessageInterface<T> {
    String getFrom();
    String getTo();
    T getContent();
}