import java.util.*;
import java.util.function.Consumer;

public class MailService<T> implements Consumer<MessageInterface<T>> {

    private Map<String, List<T>> hashMap = new HashMap<String, List<T>>(){

        public List<T> get(Object key) {
            if (this.containsKey(key)) {
                return super.get(key);
            } else {
                return Collections.emptyList();
            }
        }
    };

    @Override
    public void accept(MessageInterface<T> messageInterface) {
        List<T> list = hashMap.get(messageInterface.getTo());
        if (list.size() == 0) {
            list = new ArrayList<>();
        }
        list.add(messageInterface.getContent());
        hashMap.put(messageInterface.getTo(), list);
    }

    Map<String, List<T>> getMailBox() {
        return hashMap;
    }
}