class MailMessage extends Message<String> {

    MailMessage(String from, String to, String content) {
        super(from, to, content);
    }
}